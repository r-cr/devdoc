#!/bin/sh

INSTALL=${INSTALL:-"install"}
COPY=${COPY:-"cp -pRf"}
MKDIR=${MKDIR:-"mkdir -p"}
DESTDIR=${DESTDIR:-"${HOME}/rrr"}
PREFIX=${PREFIX:-} 

# these can not be redefined.
# Package relies on name "devdoc" and directory layout for bin and shared files inside given PREFIX.
pack_NAME="devdoc"
bin_DIR="${PREFIX}/bin"
share_DIR="${PREFIX}/share/${pack_NAME}"
share_build2markup_DIR="${PREFIX}/share/build2markup"

# these can be redefined.
DOCDIR="${DOCDIR:-${PREFIX}/share/doc/${pack_NAME}}"
MANDIR="${MANDIR:-${PREFIX}/share/man}"
ETCDIR="${ETCDIR:-/etc}"

if test -f config.sh ; then
    . config.sh
fi

install_all()
{
    ${MKDIR} \
        "${DESTDIR}${bin_DIR}/" \
        "${DESTDIR}${share_DIR}/" \
        "${DESTDIR}${share_build2markup_DIR}/" \
        "${DESTDIR}${DOCDIR}/" \
        "${DESTDIR}${ETCDIR}/" \
        "${DESTDIR}${MANDIR}/"

    ${INSTALL} -m755 bin/* "${DESTDIR}${bin_DIR}/"
    ${COPY} share/devdoc/* "${DESTDIR}${share_DIR}/"
    ${COPY} share/build2markup/* "${DESTDIR}${share_build2markup_DIR}/"
    ${INSTALL} -d man/* "${DESTDIR}${MANDIR}/"
    ${COPY} doc/man/* "${DESTDIR}${MANDIR}/"
    ${COPY} doc/html/ "${DESTDIR}${DOCDIR}/"
    ${INSTALL} -m644 'README-devdoc' 'License-ISC' 'License-latex2man' "${DESTDIR}${DOCDIR}/"
    ${INSTALL} -m644 'etc/crossref.conf' "${DESTDIR}${ETCDIR}/"
}

set -o errexit
set -o xtrace

install_all "$@"
